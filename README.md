# Canvas Impression

> The Impressionist artists abandoned the old idea that the shadow of an object was made up from the color of the object with some brown or black added. Instead, they enlivened their canvases with a new idea that the shadow of any color could be mixed from pure hues and broken up with its opposite color. For example, the shadows on a yellow surface could have some strokes of lilac painted into it to increase its vitality.

- [Impressionist color theory](http://www.artyfactory.com/color_theory/color_theory_1.htm)
- [Why Monet never used black](https://designforhackers.com/blog/impressionist-color-theory/)
- [Canvas Brushes](http://perfectionkills.com/exploring-canvas-drawing-techniques/)
- [Edge Detection](https://github.com/gabrielarchanjo/marvinj)
- [JSFeat](https://github.com/inspirit/jsfeat)
- [RGB to HSL Online Converter](https://www.rapidtables.com/convert/color/rgb-to-hsl.html)
- [HSL Color Picker Online](http://hslpicker.com/#bfbf40)
- [JS calculate complementary](https://stackoverflow.com/questions/1664140/js-function-to-calculate-complementary-colour/37657940)
- [TinyColor](https://github.com/bgrins/TinyColor)
