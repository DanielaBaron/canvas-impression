import '../styles/index.css';
import '../styles/nav.css';
import log from 'loglevel';
import main from './main';

const logger = log.getLogger('index');
logger.setLevel(log.levels.TRACE);

function handleNav(e, canvas) {
  e.preventDefault();
  const action = e.target.dataset.action;
  main[action](canvas);
}

function handleFiles(e, canvas) {
  logger.debug('e.target.files = %o', e.target.files);
  if (e.target.files && e.target.files.length > 0) {
    main.drawImage(canvas, e.target.files[0]);
  }
}

function initNav(canvas) {
  const nav = document.querySelector('.nav');
  const navItems = nav.querySelectorAll('.nav__item__link');
  navItems.forEach(navItem => {
    navItem.addEventListener('click', (e) => handleNav(e, canvas));
  });
  const fileInput = nav.querySelector('#fileInput');
  fileInput.addEventListener('change', (e) => handleFiles(e, canvas));
  return nav;
}

function init(selector) {
  const canvas = document.querySelector(selector);
  const nav = initNav(canvas);
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight - nav.clientHeight;
  logger.info(`Initialized canvas: width = ${canvas.width}, height = ${canvas.height}`);
  return canvas;
}

main.defaultDraw(init('#draw'));
