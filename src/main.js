import log from 'loglevel';
import { analyze } from './image';

const logger = log.getLogger('main');
logger.setLevel(log.levels.TRACE);

let state = {
  imageData: null
};

// TODO extract to draw module
// function drawCircle(ctx, shape) {
//   ctx.beginPath();
//   ctx.arc(shape.cx, shape.cy, shape.r, 0, 2 * Math.PI, false);
//   ctx.fillStyle = shape.c;
//   ctx.fill();
//   ctx.lineWidth = 0;
//   ctx.strokeStyle = shape.c;
//   ctx.stroke();
// }

// TODO extract to draw module
function drawRectangle(ctx, shape) {
  // ctx.fillRect(shape.x, shape.y, shape.s, shape.s)
  ctx.beginPath();
  ctx.rect(shape.x, shape.y, shape.w, shape.h);
  ctx.closePath();
  ctx.fillStyle = shape.c;
  ctx.fill();
}

// TODO extract to color module
// function* hslHue() {
//   let cur = 0;
//   while (true) {
//     yield cur;
//     if (cur === 360) {
//       cur = 0;
//     }
//     cur += 1;
//   }
// }

// TODO extract to color module
function buildColor(hsla) {
  const sat = `${hsla.sat}%`;
  const light = `${hsla.light}%`;
  const alpha = hsla.alpha;
  return `hsla(${hsla.hue}, ${sat}, ${light}, ${alpha})`;
}
// function buildColor(hueSequence) {
//   const hue = hueSequence.next().value;
//   const sat = '50%';
//   const light = '50%';
//   const alpha = 0.8;
//   return `hsla(${hue}, ${sat}, ${light}, ${alpha})`;
// }

function clear(canvas) {
  const context = canvas.getContext('2d');
  context.clearRect(0, 0, canvas.width, canvas.height);
}

function defaultDraw(canvas) {
  sat(canvas);
}

/**
 * For each hue, vary saturation holding light at 50% and alpha at 1.
 * @param {*} canvas
 */
function sat(canvas) {
  clear(canvas);
  const ctx = canvas.getContext('2d');
  const squareWidth = canvas.width / 360;
  const squareHeight = canvas.height / 100;
  for (let i = 0; i <= 360; i++) {
    for (let j = 0; j <= 100; j++) {
      let color = buildColor({ hue: i, sat: j, light: 50, alpha: 1 });
      let shape = { x: i * squareWidth, y: j * squareHeight, w: squareWidth, h: squareHeight, c: color };
      drawRectangle(ctx, shape);
    }
  }
}

/**
 * For each hue, vary light holding saturation at 100 and alpha at 1.
 * @param {*} canvas
 */
function light(canvas) {
  clear(canvas);
  const ctx = canvas.getContext('2d');
  const squareWidth = canvas.width / 360;
  const squareHeight = canvas.height / 100;
  for (let i = 0; i <= 360; i++) {
    for (let j = 0; j <= 100; j++) {
      let color = buildColor({ hue: i, sat: 100, light: j, alpha: 1 });
      let shape = { x: i * squareWidth, y: j * squareHeight, w: squareWidth, h: squareHeight, c: color };
      drawRectangle(ctx, shape);
    }
  }
}

// this could take a while - web worker?
// https://github.com/zschuessler/DeltaE
// http://zschuessler.github.io/DeltaE/demos/de76-chroma-key/#code
function analyzeImage(imageData) {
  state.imageData = imageData;
  analyze(imageData);
}

/**
 * Draw an image onto the canvas
 * https://stackoverflow.com/questions/10906734/how-to-upload-image-into-html5-canvas
 * @param {*} canvas
 * @param {*} file
 */
function drawImage(canvas, file) {
  clear(canvas);
  const ctx = canvas.getContext('2d');
  const reader = new FileReader();
  reader.onload = function (event) {
    const img = new Image();
    img.onload = function () {
      const hRatio = canvas.width / img.width    ;
      const vRatio = canvas.height / img.height  ;
      const ratio  = Math.min(hRatio, vRatio);
      const destWidth = img.width * ratio;
      const destheight = img.height * ratio;
      logger.debug(`drawImage: destWidth = ${destWidth}, destHeight = ${destheight}`);
      ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, destWidth, destheight);
      analyzeImage(ctx.getImageData(0, 0, destWidth, destheight));
    };
    img.src = event.target.result;
  };
  reader.readAsDataURL(file);
}

function process() {
  if (state.imageData) {
    analyzeImage(state.imageData);
  }
}

export default {
  defaultDraw,
  sat,
  light,
  drawImage,
  process
};
