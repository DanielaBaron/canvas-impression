import log from 'loglevel';
// import DeltaE from 'delta-e';
import tinycolor from 'tinycolor2';

const logger = log.getLogger('image');
logger.setLevel(log.levels.TRACE);

// function rgbToLab(r, g, b) {
//   var xyz = rgbToXyz(r, g, b);
//   return xyzToLab(xyz[0], xyz[1], xyz[2]);
// }

// function rgbToXyz(r, g, b) {
//   let _r = (r / 255);
//   let _g = (g / 255);
//   let _b = (b / 255);

//   if (_r > 0.04045) {
//     _r = Math.pow(((_r + 0.055) / 1.055), 2.4);
//   }
//   else {
//     _r = _r / 12.92;
//   }

//   if (_g > 0.04045) {
//     _g = Math.pow(((_g + 0.055) / 1.055), 2.4);
//   }
//   else {
//     _g = _g / 12.92;
//   }

//   if (_b > 0.04045) {
//     _b = Math.pow(((_b + 0.055) / 1.055), 2.4);
//   }
//   else {
//     _b = _b / 12.92;
//   }

//   _r = _r * 100;
//   _g = _g * 100;
//   _b = _b * 100;

//   let X = _r * 0.4124 + _g * 0.3576 + _b * 0.1805;
//   let Y = _r * 0.2126 + _g * 0.7152 + _b * 0.0722;
//   let Z = _r * 0.0193 + _g * 0.1192 + _b * 0.9505;

//   return [X, Y, Z];
// }

// function xyzToLab(x, y, z) {
//   let ref_X = 95.047;
//   let ref_Y = 100.000;
//   let ref_Z = 108.883;

//   let _X = x / ref_X;
//   let _Y = y / ref_Y;
//   let _Z = z / ref_Z;

//   if (_X > 0.008856) {
//     _X = Math.pow(_X, (1 / 3));
//   }
//   else {
//     _X = (7.787 * _X) + (16 / 116);
//   }

//   if (_Y > 0.008856) {
//     _Y = Math.pow(_Y, (1 / 3));
//   }
//   else {
//     _Y = (7.787 * _Y) + (16 / 116);
//   }

//   if (_Z > 0.008856) {
//     _Z = Math.pow(_Z, (1 / 3));
//   }
//   else {
//     _Z = (7.787 * _Z) + (16 / 116);
//   }

//   let CIE_L = (116 * _Y) - 16;
//   let CIE_a = 500 * (_X - _Y);
//   let CIE_b = 200 * (_Y - _Z);

//   // return [CIE_L, CIE_a, CIE_b];
//   return {L: CIE_L, A: CIE_a, B: CIE_b};
// }

/**
 * https://github.com/zschuessler/DeltaE
 * http://zschuessler.github.io/DeltaE/learn/
 *    Delta E	  Perception
 *   =================================================
 *    <= 1.0	  Not perceptible by human eyes.
 *    1 - 2	    Perceptible through close observation.
 *    2 - 10	  Perceptible at a glance.
 *   11 - 49	  Colors are more similar than opposite
 *  100	        Colors are exact opposite
 * @param {*} rgbMap
 */
// function findSimilar(rgbMap) {
//   for (let outerKey of rgbMap.keys()) {
//     for (let innerKey of rgbMap.keys()) {
//       if (outerKey !== innerKey) {
//         let [outerR, outerG, outerB] = outerKey.split('-');
//         let outerLab = rgbToLab(outerR, outerG, outerB);
//         let [innerR, innerG, innerB] = innerKey.split('-');
//         let innerLab = rgbToLab(innerR, innerG, innerB);
//         let delta = DeltaE.getDeltaE00(outerLab, innerLab);
//         logger.debug('findSimilar: outerKey %o to innerKey %0 diff = %o', outerKey, innerKey, delta);
//       }
//     }
//   }
// }

function buildRGBMap(data) {
  const rgbMap = new Map();
  let pixelCounter = 0;
  // every 4 numbers represents r/g/b and opacity
  for (let i=0; i < data.length; i += 4) {
    let red = data[i];
    let green = data[i+1];
    let blue = data[i+2];
    // let alpha = data[i+3];
    let rgbKey = `${red}-${green}-${blue}`;
    if (rgbMap.get(rgbKey)) {
      let curPixelSet = rgbMap.get(rgbKey);
      curPixelSet.add(pixelCounter);
    } else {
      let pixelSet = new Set();
      pixelSet.add(pixelCounter);
      rgbMap.set(rgbKey, pixelSet);
    }
    pixelCounter += 1;
  }
  logger.debug(`buildRGBMap: pixelCounter = ${pixelCounter}`);
  return rgbMap;
}

function processColor(rgbMap) {
  const complementMap = new Map();
  rgbMap.forEach((val, curRgb) => {
    let [r, g, b] = curRgb.split('-');
    let tc = tinycolor({r, g, b});
    let hsl = tc.toHsl();
    let pure = tinycolor({h: hsl.h, s: 0.5, l: 0.5, a: 1});
    let complement = pure.complement();
    complementMap.set(tc.toRgb(), {pure: pure.toRgb(), complement: complement.toRgb()});
  });
  return complementMap;
}

export function analyze(imageData) {
  const data = imageData.data;
  const rgbMap = buildRGBMap(data);
  logger.debug('analyze: rgbMap = %o', rgbMap);
  // findSimilar(rgbMap);
  const complementMap = processColor(rgbMap);
  logger.debug('analyze: complementMap = %o', complementMap);
}
